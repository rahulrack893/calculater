import React, {Component} from 'react'
import Container from '@material-ui/core/Container';
import './Calculater.css'

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import Button from '@material-ui/core/Button';



class Calculater extends Component {

   
    
    render() {
        return (
            <React.Fragment>
                <Container maxWidth="sm">

        <Grid container spacing={3} className="cal_container">
        <Grid item xs={12} >
          <Paper className='paper cal_screen'>xs=12</Paper>
        </Grid>
        
        <Grid item xs={3}>
          
          <Button className='paper num_btn'>1</Button>
        </Grid>
        <Grid item xs={3}>
           <Button className='paper num_btn'>2</Button>
        </Grid>
        <Grid item xs={3}>
          <Button className='paper num_btn'>3</Button>
        </Grid>
        <Grid item xs={3}>
          <Button className='paper num_btn'>/</Button>
        </Grid>
        <Grid item xs={3}>
          
          <Button className='paper num_btn'>4</Button>
        </Grid>
        <Grid item xs={3}>
           <Button className='paper num_btn'>5</Button>
        </Grid>
        <Grid item xs={3}>
          <Button className='paper num_btn'>6</Button>
        </Grid>
        <Grid item xs={3}>
          <Button className='paper num_btn'>*</Button>
        </Grid>
        <Grid item xs={3}>
          
          <Button className='paper num_btn'>7</Button>
        </Grid>
        <Grid item xs={3}>
           <Button className='paper num_btn'>8</Button>
        </Grid>
        <Grid item xs={3}>
          <Button className='paper num_btn'>9</Button>
        </Grid>
        <Grid item xs={3}>
          <Button className='paper num_btn'>-</Button>
        </Grid>
           <Grid item xs={3}>
          
          <Button className='paper num_btn'>0</Button>
        </Grid>
        <Grid item xs={3}>
           <Button className='paper num_btn'>^</Button>
        </Grid>
        <Grid item xs={3}>
          <Button className='paper num_btn'>=</Button>
        </Grid>
        <Grid item xs={3}>
          <Button className='paper num_btn'>+</Button>
        </Grid>
      </Grid>
      
                </Container>
               
            </React.Fragment>
        )
    }
}
export default Calculater